package com.androsor99.view;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsolePrinter implements Printer {

    private static final ConsolePrinter INSTANCE = new ConsolePrinter();

    @Override
    public void print(String line) {
        System.out.println(line);
    }

    public static ConsolePrinter getInstance() {
        return INSTANCE;
    }
}
