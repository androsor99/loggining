package com.androsor99.view;

public interface Printer {

    void print(String line);
}
