package com.androsor99.entity;

import lombok.Data;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

@Data
public class ConfigFile {

     String suffix = "";
     List<Path> filesPath = Collections.emptyList();
}
