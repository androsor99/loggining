package com.androsor99.service;

import com.androsor99.dao.ConfigFileFromJsonDao;
import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.exception.ServiceException;
import com.androsor99.validator.FileValidator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RenamingBySuffixService implements Service {

    private static final RenamingBySuffixService INSTANCE = new RenamingBySuffixService();
    private static final Logger logger = LogManager.getLogger(RenamingBySuffixService.class);

    private final ConfigFileFromJsonDao file = ConfigFileFromJsonDao.getInstance();
    private final FileValidator fileValidator = FileValidator.getInstance();

    public String rename() throws ServiceException {
        try {
            ConfigFile configFile = file.create();
            return setMessage(configFile);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private String setMessage(ConfigFile configFile) {
        if (fileValidator.isEmpty(configFile)) {
            logger.error("The config file does not contain a set of files");
            return "";
        } else {
            return renameAllFiles(configFile);
        }
    }

    private String renameAllFiles(ConfigFile configFile) {
        String suffix = configFile.getSuffix();
        List<Path> files = configFile.getFilesPath();
        StringBuilder result = new StringBuilder();
        for (Path filePath : files) {
            String oldFileName = getNameFile(filePath);
            String newFileName = addSuffixToName(oldFileName, suffix);
            Path filePathWithNewName = Paths.get(filePath.getParent().toString(), newFileName);
            boolean fileExist = fileValidator.isFileExist(filePath);
            boolean newFileExist = fileValidator.isFileExist(filePathWithNewName);
            if (fileExist && !newFileExist) {
                result.append(renameFile(filePath, oldFileName, newFileName));
            } else if (newFileExist) {
                logger.error("The file \"{}\" is not renamed. Invalid new name \"{}\". A file with the same name already exists.", oldFileName, newFileName);
            } else {
                logger.error("The file {} does not exist", oldFileName);
            }
        }
        return result.toString();
    }

    private String getNameFile(Path path) {
        return path.getFileName().toString();
    }

    private String addSuffixToName(String name, String suffix) {
        return String.join(suffix, name.substring(0, name.lastIndexOf(".")), name.substring(name.lastIndexOf(".")));
    }

    private String renameFile(Path filePath, String oldFileName, String newFileName) {
        String renameMessage = "";
        try {
            move(filePath, newFileName);
            renameMessage = String.format("Old name file \"%s\" -> New name file \"%s\"\n", oldFileName, newFileName);
            logger.info("File \"{}\" renameMessage successfully.", oldFileName);
        } catch (IOException e) {
            logger.fatal("The file has not been renamed", new ServiceException(e));
        }
        return renameMessage;
    }

    private void move(Path path, String fileName) throws IOException {
        Files.move(path, path.resolveSibling(fileName), REPLACE_EXISTING);
    }

    public static RenamingBySuffixService getInstance() {
        return INSTANCE;
    }
}
