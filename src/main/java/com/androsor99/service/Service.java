package com.androsor99.service;

import com.androsor99.exception.ServiceException;

public interface Service {

    String rename() throws ServiceException;
}
