package com.androsor99.controller;

import com.androsor99.exception.ServiceException;
import com.androsor99.service.RenamingBySuffixService;
import com.androsor99.view.ConsolePrinter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RenamingBySuffixController implements Controller {

    private static final RenamingBySuffixController INSTANCE = new RenamingBySuffixController();

    private final RenamingBySuffixService service = RenamingBySuffixService.getInstance();
    private final ConsolePrinter printer = ConsolePrinter.getInstance();


    @Override
    public void doAction() throws ServiceException {
        printer.print(service.rename());
    }

    public static RenamingBySuffixController getInstance() {
        return INSTANCE;
    }
}
