package com.androsor99.dao;

import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.util.PropertiesUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigFileFromJsonDao implements Dao<ConfigFile> {

    private static final String CONFIG_FILE_KEY = "config_file";
    private static final ConfigFileFromJsonDao INSTANCE = new ConfigFileFromJsonDao();
    private static final Logger logger = LogManager.getLogger(ConfigFileFromJsonDao.class);

    @Override
    public ConfigFile create() throws DaoException {
        try (FileInputStream fileInputStream = new FileInputStream(PropertiesUtil.get(CONFIG_FILE_KEY))) {
            ObjectMapper mapper = new ObjectMapper();
            ConfigFile configFile = mapper.readValue(fileInputStream, ConfigFile.class);
            logger.info("The {} was successfully read", CONFIG_FILE_KEY);
            return configFile;
        } catch (IOException | IllegalArgumentException e) {
            logger.fatal("The {} was not read", CONFIG_FILE_KEY, e);
            throw new DaoException(e);
        }
    }

    public static ConfigFileFromJsonDao getInstance() {
        return INSTANCE;
    }
}
