package com.androsor99.dao;

import com.androsor99.exception.DaoException;

public interface Dao<T> {

    T create() throws DaoException;
}
