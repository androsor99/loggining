## 4. Loggining

### Home task [SuffixingApp Logging]

### _Description_

Get Suffixing App project from Maven topic. Add logging.

### _App Specification_

It is a Suffixing App - a small java application that refers to a config file and renames a set of files and renames them adding a suffix specified in the same config.

### _Details:_
* Application should read a config file on the startup
* Then it should ensure that all files from the config exist
* Then it should rename each file adding a suffix from the config to its name

### _Logging Specification_
* Application should log startup information.
* Application should log information on config read.
* Application should log renaming process information.
* Application should log summary information.
* Application should log shutdown information.
* Application should handle and log possible errors.

Use different logging level. All log entries should contain a date and time information as well.

### _Steps_

1. Complete the project to meet specifications.
2. Show the mentor your results.
